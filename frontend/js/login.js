window.onload = function () {
    const signUpButton = document.getElementById('signUp');
    const signInButton = document.getElementById('signIn');
    const container = document.getElementById('container');

    signUpButton.addEventListener('click', () => {
        container.classList.add("right-panel-active");
    });

    signInButton.addEventListener('click', () => {
        container.classList.remove("right-panel-active");
    });
}

$(document).ready(function() {

  

    $("#login").submit(function (e) {
        e.preventDefault();
        login();
    })

    $("#createAccountForm").submit(function (e) {
        e.preventDefault();
        register();
    })
});


function login() {
    $.ajax({
        url: "http://localhost:42069/api/customer/login",
        type: "POST",
        data: JSON.stringify({
            email: $("#loginEmail").val(),
        }),
        async: true,
        contentType: "application/json",
        success: successCallback,
        error: errorLoginCallback
    });
}

function register() {

    $.ajax({
        url: 'http://localhost:42069/api/customer',
        type: 'POST',
        data: JSON.stringify({
            firstName: $("#firstName").val(),
            lastName: $("#lastName").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
        }),

        async: true,
        contentType: 'application/json',
        success: successCallback,
        error: errorCreateCallback

    });

}


function successCallback(response) {

    var id = response.id;
    sessionStorage.setItem("id", id)
    sessionStorage.setItem("AuthenticationState", "Authenticated");
    
    window.location.replace("../views/sellBagsPage.html");
    
}
function errorLoginCallback() {
    alert("Your email could not be found in the system");
}

function errorCreateCallback() {

    alert("An error has occurred. Please try again later");

}









