let fileInput = document.getElementsByName("upload");

let postSuccess = false;
let case_id = null;

function renameFile(originalFile, newName) {
    return new File([originalFile], newName, {
        type: originalFile.type,
        lastModified: originalFile.lastModified,
    });
}

$(function postBag() {
    $("#contact_form").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "http://localhost:42069/api/bag/",
            type: "POST",
            data: JSON.stringify({
                customerId: sessionStorage.getItem("id"),
                brand: $("#brand").val(),
                condition: $("#condition").val(),
                size: $("#size").val(),
                comments: $("#notes").val(),
                extras: {
                    box: $("#ff-box").val(),
                    authentic_card: $("#ff-authentic-card").val(),
                    shoulder_strap: $("#ff-shoulder-strap").val(),
                    dustbag: $("#ff-dustbag").val(),
                    pouch: $("#ff-pouch").val(),
                    padlock_key: $("#ff-padlock-key").val(),
                    bag_charm: $("#ff-bag-charm").val(),
                    name_tag: $("#ff-name-tag").val(),
                    mirror: $("ff-mirror").val()
                }
            }),
            async: true,
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: postImage,
            error: errorCallback
        });
    });
});

function postImage(response) {
    case_id = response.id;
    let bag_id = response.bagId;

    let serialCodeFile = $("#serialcodefile").prop('files')[0];
    let serialCodeBlob = renameFile(serialCodeFile, "serialcode.png");

    /*let frontFile = $("#frontfile").prop('files')[0];
    let backFile = $("#backfile").prop('files')[0];
    let insideFile = $("#insidefile").prop('files')[0];
    let stampFile = $("#stampfile").prop('files')[0];
    let damageFile = $("#stampfile").prop('files')[0];

    let serialCodeBlob = renameFile(serialCodeFile, "serialcode.png");
    let frontFileBlob = renameFile(serialCodeFile, "frontfile.png");
    let backFileBlob = renameFile(serialCodeFile, "backfile.png");
    let insideFileBlob = renameFile(serialCodeFile, "insidefile.png");
    let stampFileBlob = renameFile(serialCodeFile, "stampfile.png");
    let damageFileBlob = renameFile(serialCodeFile, "damagefile.png"); 

    checkNotNullAndPost(serialCodeFile, "serialcode.png");
    checkNotNullAndPost(frontFile, "frontfile.png");
    checkNotNullAndPost(backFile, "backfile.png");
    checkNotNullAndPost(insideFile, "insideFile.png");
    checkNotNullAndPost(stampFile, "stampfile.png");
    checkNotNullAndPost(damageFile, "damagefile.png");

    function checkNotNullAndPost(file, newFileName) {
        if (file !== undefined) {
            file = renameFile(newFileName);

            let formData = new FormData();
            formData.append("file", file, file.name);

            $.ajax({
                url: "http://localhost:8080/api/bag/image/" + bag_id,
                type: "POST",
                data: formData,
                processData: false,
                contentType: 'multipart/form-data',
                enctype: 'multipart/form-data',
                contentType: false,
                async: true,
                error: errorCallback
            });
        }
    }
    
    */


    let formData = new FormData();
    formData.append("file", serialCodeBlob, serialCodeBlob.name);

    $.ajax({
        url: "http://localhost:42069/api/bag/image/" + bag_id,
        type: "POST",
        data: formData,
        processData: false,
        contentType: 'multipart/form-data',
        enctype: 'multipart/form-data',
        contentType: false,
        async: true,
        success: onSuccess,
    });
}


function onSuccess() {
    alert("Your case id is: " + case_id);
    window.location.href = "http://localhost:3000/";

}

function errorCallback(request, status, error) {
    alert("Something went wrong. Please try again");
}

