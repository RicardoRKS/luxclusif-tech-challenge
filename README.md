This project was developed for Luxclusif tech challenge.

Backend is fully developed in Java using Spring Boot framework, hibernate and mySQL.
Backend testing is performed using jUnit and Mockito.
Google API is used to upload data to a dedicated spreadsheet.

Frontend is developed in HTML, CSS and Javascript. There's also a portion made in React as a showcase.

Optional Requirements: mySQL (latest version). I use a H2 in-memory database for dev and a mySQL for prod. Don't worry if you don't have mySQL installed!

Usage: Clone or download the code to your own computer.
You will need a google account for uploading to drive and spreadsheets. Use the following one:

    email: luxclusiftechchallenge@gmail.com
    password: techchallenge

Use your favorite IDE to startup the backend server and leave it running. The default port used to host the server is 8080, so please make sure you do not have any other tomcat project online that might interfere with this server.

   There's a jar file in the folder. You can try running this one directly.
`java -jar demo-0.0.1-SNAPSHOT-exec.jar`
   This might cause issues in Windows (I developed the app in Linux) such as: Java Runtime only recognizes class file versions up to 52.0.


The frontend is divided in two parts. One is being hosted by webhostapp in: https://luxclusifchallenge.000webhostapp.com/
If the website doesn't work (because it's free and they do have some downtime) you can simply open the html files in frontend!

The other portion, the React one, can be launched by accessing the "react" directory on the command line and write: 

    npm install
    npm start

This will launch the react server on port 3000. Then again, make sure no other app is deployed at this port to prevent interference.




Landing Page: https://luxclusifchallenge.000webhostapp.com/ 
Here you have all the information required to proceed with selling your bag! In case you're interested, click the "Start Selling" button.

You will have to be logged in to access the remaining pages, so just create a mock account and proceed.

You can later test the Sign in process with this same account.

Once logged in and if everything went right, you should be in the Bag selling page!

Fill the required information (You won't be able to advance unless you do so!).

After submitting, a popup with you case id should show up (might take a few seconds)!

You wil be redirected to the React portion of this challenge, where you can check customer and bag information that is fetched from the backend database.

Congratulations! You finished the project tour. Feel free to break my code in any way you can think of and let me know so I can improve this project!


KNOWN ISSUES: 
    File uploading is limited. Uploading several files failed often. I decided to leave only one file that can be uploaded (serial code) so that it is possible to upload to drive.
