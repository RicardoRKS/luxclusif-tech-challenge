import axios from 'axios';

const Customer_API = "http://localhost:42069/api/customer"
const Case_API = "http://localhost:42069/api/case";

export async function fetchUserDetails(id) {
    const [profile] = await Promise.all([
        getProfile(id, Customer_API)
    ]);

    return {
        profile
    }
}

export async function fetchCaseIDDetails(id) {
    const [profile] = await Promise.all([
        getProfile(id, Case_API)
    ]);

    return {
        profile
    }
}

async function getProfile(id , api_url) {
    const url = `${api_url}/${id}`;

    const response = await axios.get(url);
    return response.data;
}

