import React from 'react';
import 'semantic-ui-react';
import { Container } from 'semantic-ui-react';
import Customer from './components/customer/customer';
import Case from './components/CaseID/CaseID';
import CorePage from './components/core/core-page';
import { BrowserRouter } from 'react-router-dom';




export function App() {
  return (
    <BrowserRouter>
      <Container>
        <CorePage />
        <Customer />
        <Case />
      </Container>
    </BrowserRouter>

  );
}

export default App;
