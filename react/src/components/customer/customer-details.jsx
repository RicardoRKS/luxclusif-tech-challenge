import React from 'react';
import PropTypes from 'prop-types';
import { Statistic } from 'semantic-ui-react';
import ProfileCard from '../core/profile-card';

function CustomerDetails({ profile }) {
    return (
        <ProfileCard
            header={profile.firstName}
        >

            <Statistic size="tiny">
                <Statistic.Label>Balance</Statistic.Label>
                <Statistic.Value>{profile.balance}</Statistic.Value>
            </Statistic>
            <Statistic size="tiny">
                <Statistic.Label>Bag Number</Statistic.Label>
                <Statistic.Value>{profile.bagNumber}</Statistic.Value>
            </Statistic>
        </ProfileCard>
    );
}

CustomerDetails.propTypes = {
    profile: PropTypes.shape({
        firstName: PropTypes.string.isRequired,
        lastName: PropTypes.string.isRequired,
        email: PropTypes.string,
        phone: PropTypes.string,
        balance: PropTypes.string.isRequired,
        bagNumber: PropTypes.number.isRequired,
    }).isRequired
};

export default CustomerDetails;