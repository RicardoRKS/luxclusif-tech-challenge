import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form } from 'semantic-ui-react';

class Input extends React.Component {
    static propTypes = {
        onSubmit: PropTypes.func.isRequired,
        text: PropTypes.string.isRequired
    };

    state = {
        value: ''
    };

    onChange = event =>
        this.setState({
            value: event.target.value
        });

    onSubmit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state.value);
        this.setState({
            value: ''
        });
    };

    render() {
        const { value } = this.state;
        const { text } = this.props;

        return (
            <Form size="big" onSubmit={this.onSubmit}>
                <Form.Field>
                    <label htmlFor="value">{text}</label>
                    <input
                        id="value"
                        type="number"
                        value={value}
                        onChange={this.onChange}
                    />
                </Form.Field>
                <Button type="submit" size="big" disabled={!value}>
                    Submit
                </Button>
            </Form>
        );
    }
}

export default Input;
