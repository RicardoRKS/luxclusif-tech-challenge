import React from 'react';
import { fetchUserDetails } from "../../services/details-fetcher";
import Input from './input';
import CustomerDetails from "./customer-details"
import { Grid } from 'semantic-ui-react';
import Loading from '../core/loading';
import Error from '../core/error';

class Customer extends React.Component {
    state = {
        profile: null,
        error: null,
        loading: null
    };

    handleSubmit = async id => {
        this.setState({
            loading: true
        });

        try {
            const userDetails = await fetchUserDetails(id);

            this.setState({
                profile: {
                    id,
                    firstName: userDetails.profile.firstName,
                    lastName: userDetails.profile.lastName,
                    email: userDetails.profile.email,
                    phone: userDetails.profile.phone,
                    balance: userDetails.profile.balance,
                    bagNumber: userDetails.profile.bagNumber
                },
                error: null,
                loading: null
            });
        } catch (error) {
            this.setState({
                error: error.message,
                profile: null
            });
        }
    };

    render() {
        let { profile, loading, error } = this.state;
        return (
            <Grid columns={1} textAlign="center">
                <Grid.Row>
                    <Input
                        text="Second Life Customer ID"
                        onSubmit={this.handleSubmit}
                    />
                </Grid.Row>
                <Grid.Row>
                    {loading ? (
                        <Loading header="Loading customer details..." />
                    ) : (
                        profile && <CustomerDetails profile={profile} />
                    )}
                </Grid.Row>
                {error && <Error message={error} />}
            </Grid>
        );
    }
}

export default Customer;