import React from 'react';
import Input from '../customer/input';
import { fetchCaseIDDetails } from "../../services/details-fetcher";
import { Grid } from 'semantic-ui-react';
import { Button } from 'semantic-ui-react';
import Loading from '../core/loading';
import Error from '../core/error';
import CaseIDDetails from './caseID-details';

class CaseID extends React.Component {

    state = {
        profile: null,
        error: null,
        loading: null
    };

    handleSubmit = async id => {
        this.setState({
            loading: true
        });

        try {
            const caseIDDetails = await fetchCaseIDDetails(id);

            this.setState({

                profile: {
                    id,
                    firstName: caseIDDetails.profile.firstName,
                    lastName: caseIDDetails.profile.lastName,
                    email: caseIDDetails.profile.email,
                    brand: caseIDDetails.profile.brand,
                    condition: caseIDDetails.profile.condition,
                    size: caseIDDetails.profile.size,
                    box: (caseIDDetails.profile.box = 1 ? "Yes" : "No"),
                    authenticityCard: (caseIDDetails.profile.authenticityCard === 1 ? "Yes" : "No"),
                    shoulderStrap: (caseIDDetails.profile.shoulderStrap === 1 ? "Yes" : "No"),
                    dustbag: (caseIDDetails.profile.dustbag === 1 ? "Yes" : "No"),
                    pouch: (caseIDDetails.profile.pouch === 1 ? "Yes" : "No"),
                    padlockAndKey: (caseIDDetails.profile.padlockAndKey === 1 ? "Yes" : "No"),
                    bagcharm: (caseIDDetails.profile.bagcharm === 1 ? "Yes" : "No"),
                    nameTag: (caseIDDetails.profile.nameTag === 1 ? "Yes" : "No"),
                    mirror: (caseIDDetails.profile.mirror === 1 ? "Yes" : "No"),
                },
                error: null,
                loading: null,
            });
        } catch (error) {
            this.setState({
                error: error.message,
                loading: null,
                profile: null
            });
        }
    };

    render() {
        let { profile, loading, error } = this.state;
        return (
            <Grid columns={1} textAlign="center">
                <Grid.Row>
                    <Input
                        text="Second Life Case ID Details"
                        onSubmit={this.handleSubmit}
                    />
                </Grid.Row>
                <Grid.Row>
                    {loading ? (
                        <Loading header="Loading caseID details..." />
                    ) : (
                            profile && <CaseIDDetails profile={profile} />
                        )}
                </Grid.Row>
                {error && <Error message={error} />}
                <Grid.Row>
            <Button href="https://luxclusifchallenge.000webhostapp.com/">
                        Go back to main
            </Button>
                </Grid.Row>
            </Grid>
        );
    }
}

export default CaseID;