import React from 'react';

import { Statistic } from 'semantic-ui-react';
import ProfileCard from '../core/profile-card';

function CaseIDDetails({ profile }) {
    return (
        <ProfileCard
            header= {profile.id} 
        >

            <Statistic size="tiny">
                <Statistic.Label>Customer Name</Statistic.Label>
                <Statistic.Value>{profile.firstName}</Statistic.Value>
            </Statistic>
            <Statistic size="tiny">
                <Statistic.Label>Bag Brand</Statistic.Label>
                <Statistic.Value>{profile.brand}</Statistic.Value>
            </Statistic>

            <h3>
                More Customer details:
            </h3>
            <p>
                Last Name: {profile.lastName}
                <br/>  
                Email: {profile.email}
            </p>
            <h3>
                Bag Details:
            </h3>
            <p>
                Condition: {profile.condition} 
                <br/>  
                Size: {profile.size}
            </p>
            <h3>
                Extras:
            </h3>
            <p>
                Box: {profile.box}
                <br/>  
                Authenticity Card: {profile.authenticityCard}
                <br/>  
                Shoulder Strap: {profile.shoulderStrap}
                <br/>  
                Dustbag: {profile.dustbag}
                <br/>  
                Pouch: {profile.pouch}
                <br/>  
                Padlock and Key: {profile.padlockAndKey}
                <br/>  
                Bagcharm: {profile.bagcharm}
                <br/>  
                Name Tag: {profile.nameTag}
                <br/>  
                Mirror: {profile.mirror}

            </p>
        </ProfileCard>
    );
}

export default CaseIDDetails;