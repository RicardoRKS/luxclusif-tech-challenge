import React from "react"

class CorePage extends React.Component {
    render() {
        return (
            <div>
                <style type="text/css" dangerouslySetInnerHTML={{ __html: "\n    /** changes */\n    #email.readonly {\n      background-color: #e9ecefb3 !important;\n      color: #6c757df0 !important;\n    }\n\n    .sms-checkbox {\n      padding-top: 53px;\n    }\n\n    div.heromask {\n      background-size: cover !important;\n      background-repeat: no-repeat !important;\n      background-position: center 42% !important;\n    }\n\n    p.ff-section a {\n      text-decoration: underline;\n      color: #000000;\n    }\n\n    .p-guide {\n      font-family: 'Polaris-Bold';\n      text-decoration: underline;\n      color: #000;\n    }\n\n    .polar-bold {\n      font-family: 'Polaris-Bold' !important;\n    }\n\n    .p-guide:hover {\n      color: #000;\n    }\n\n    .iti {\n      display: block;\n    }\n\n    #userForm {\n      padding: 0 60px;\n    }\n\n    input[disabled],\n    select[disabled] {\n      cursor: no-drop;\n    }\n\n    span.messages {\n      font-size: 0.8rem;\n      color: #E91E63;\n    }\n\n    .ff-country .bootstrap-select button {\n      background: #fff;\n      width: 100% !important;\n      padding: .375rem .75rem;\n      font-size: 1rem;\n      line-height: 1.5;\n      color: #495057;\n      background-color: #fff;\n      background-clip: padding-box;\n      border-radius: .25rem;\n      height: 38px;\n    }\n\n    .ff-country .bootstrap-select button:focus {\n      color: #495057;\n      background-color: #fff;\n      border-color: #80bdff;\n      box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);\n    }\n\n    #ff-sell-form-x22 strong {\n      font-family: 'Polaris-bold';\n    }\n\n    .sc-bold {\n      font-family: 'Polaris-bold';\n    }\n\n    img.imgremove {\n      height: 30px;\n      width: 30px;\n      opacity: 0.5;\n      position: absolute;\n      top: 3%;\n      right: 3%;\n      display: none;\n    }\n\n    .ff-multi-upload label {\n      text-decoration: underline;\n      cursor: pointer;\n    }\n\n    .ff-multi-upload,\n    .ff-multi-upload label input.mfiles {\n      display: none;\n    }\n\n    .uploadFileWrapper .progress {\n      position: absolute;\n      width: 95%;\n      bottom: 45%;\n      margin-left: auto;\n      margin-right: auto;\n      left: 0;\n      right: 0;\n      border: 1px solid gray;\n      display: none;\n    }\n\n    .uploadFileWrapper.uploading>.img-fluid {\n      opacity: 0.2;\n    }\n\n    .cb-sms-span {\n      height: 18px;\n      width: 18px;\n      background: #fff;\n      border: 1px solid gray;\n      position: absolute;\n      top: 0;\n      left: 0;\n    }\n\n    .cb-sms-label {\n      font-size: 0.8em;\n      cursor: pointer;\n      position: relative;\n    }\n\n    .cb-sms-label input {\n      opacity: 0;\n      margin-left: 10px;\n    }\n\n    .cb-sms-label input:checked~.cb-sms-span {\n      background: #fff;\n    }\n\n    .cb-sms-label input:checked~.cb-sms-span:after {\n      left: 5px;\n      top: 1px;\n      width: 6px;\n      height: 12px;\n      border: solid #222222;\n      border-width: 0 3px 3px 0;\n      -webkit-transform: rotate(45deg);\n      -ms-transform: rotate(45deg);\n      transform: rotate(45deg);\n      content: '';\n      position: absolute;\n      display: block;\n    }\n\n    .center {\n      display: block;\n      text-align: -webkit-center;\n    }\n\n    div.col-lg-12.mp-exclude {\n      padding-right: 15px !important;\n      padding-left: 15px !important;\n    }\n\n    #additional-bag .to-copy {\n      border-top: 2px solid lightgray;\n      padding-top: 25px;\n    }\n\n    .add-bag {\n      cursor: pointer;\n      color: #adb5bd;\n    }\n\n    .btn.btn-hover:focus,\n    .btn.btn-hover:active,\n    .btn.btn-hover:hover {\n      background-color: #fff !important;\n    }\n\n    .btn.btn-outline-dark:focus,\n    .btn.btn-outline-dark:active,\n    .btn.btn-outline-dark:hover {\n      color: #fff !important;\n      background-color: #000 !important;\n    }\n\n    #photoGuidelineWrapper {\n      padding-left: 60px;\n      padding-right: 60px;\n      padding-bottom: 65px;\n    }\n\n    #photoGuidelineModal .modal-body {\n      overflow-x: hidden;\n      overflow-y: scroll;\n      max-height: 500px;\n    }\n\n    #photoGuidelineModal .modal-footer {\n      border: 1px solid #fff;\n      width: 100%;\n      background-color: white;\n    }\n\n    #photoGuidelineModal .modal-footer.ff-section {\n      margin-bottom: 10px;\n      margin-top: 10px;\n    }\n\n    #photoGuidelineModal .modal-header h2 {\n      font-size: 24px;\n      color: #000;\n    }\n\n    @media (max-width: 767px) {\n      #userForm {\n        padding: 0 30px;\n      }\n\n      .sms-checkbox {\n        padding-top: 0px;\n      }\n\n      .bootstrap-select button.btn {\n        width: 100% !important;\n      }\n    }\n\n    #conditionClick {\n      font-family: Polaris-Bold, sans-serif;\n      color: #000;\n      cursor: pointer;\n    }\n  " }} />
                <section>
                    <div className="ff-header d-flex justify-content-center">
                        <a href="http://localhost:5501/index.html" className="d-flex p-2 my-3 justify-content-center we-are-here">
                            <img className="ff-logo align-self-center" alt="Farfetch Secondlife" src={require("../../img/logo/logo-horizontal.svg")} /></a>
                    </div>
                </section>
                <section>
                    <style dangerouslySetInnerHTML={{ __html: "\n      .ff-h1 {\n        margin-bottom: 24px;\n        font-family: 'Polaris-Bold';\n        font-size: 36.0px;\n      }\n\n      .heromask2 {\n        width: 92%;\n        margin: auto;\n        margin-bottom: 70px;\n      }\n\n      .heromask2 img {\n        width: 100%;\n      }\n\n      @media (max-width: 767px) {\n        .heromask2 {\n          display: none;\n        }\n      }\n\n      #img-div .bg-webp-img {\n        width: 100%;\n        height: 100%;\n        background-repeat: no-repeat;\n        background-size: cover;\n        background-position: center;\n        background-color: #ed723f;\n      }\n    " }} />
                    <div className="heromask2">
                        <div className="d-flex p-2 my-3 justify-content-center we-are-here" style={{ backgroundColor: '#ececec' }}>
                            <p className="m-0"><span className="font-p-bold">We are here for you</span> <span className="px-1">|</span> Farfetch Second
                Life is continuing to pick up your items safely from home</p>
                        </div>
                        <picture>
                            <source type="image/jpeg" srcSet={require("../../img/banners/ff-desktop.jpg")} />
                            <img src={require("../../img/banners/ff-desktop.jpg")} alt="September Banner" className=" " />
                        </picture>
                    </div>
                </section>

                <script src="https://unpkg.com/react/umd/react.production.min.js" crossOrigin="true"></script>

                <script src="https://unpkg.com/react-dom/umd/react-dom.production.min.js" crossOrigin="true"></script>

                <script src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js" crossOrigin="true"></script>

            </div>
        );
    }
};

export default CorePage;