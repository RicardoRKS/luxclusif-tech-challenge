import React from 'react';
import PropTypes from 'prop-types';
import { Message, Icon } from 'semantic-ui-react';

function Loading({ header, message }) {
    return (
        <Message icon compact size="large">
            <Icon name="circle notched" loading />
            <Message.Content>
                <Message.Header>{header}</Message.Header>
                {message}
            </Message.Content>
        </Message>
    );
}

Loading.propTypes = {
    header: PropTypes.string.isRequired,
    message: PropTypes.string
};

export default Loading;