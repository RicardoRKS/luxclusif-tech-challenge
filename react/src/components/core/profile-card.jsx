import React from 'react';
import PropTypes from 'prop-types';
import { Card, Label, Image } from 'semantic-ui-react';

function ProfileCard({label, avatar, header, description, children }) {
    return (
        <Card raised >
            <Card.Content>
                {label && <Label attached="top left">{label}</Label>}
                <Image src={avatar} circular />
                <Card.Header>{header}</Card.Header>
                {description && (
                    <Card.Description>{description}</Card.Description>
                )}
                <Card.Content extra>{children}</Card.Content>
            </Card.Content>
        </Card>
    );
}

ProfileCard.propTypes = {
    header: PropTypes.string.isRequired,
    label: PropTypes.string
};

export default ProfileCard;