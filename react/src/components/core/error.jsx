import React from 'react';
import PropTypes from 'prop-types';
import { Message } from 'semantic-ui-react';

function Error({ message }) {
    return (
        <Message negative size="large">
            <Message.Header>{message}</Message.Header>
        </Message>
    );
}

Error.propTypes = {
    message: PropTypes.string.isRequired
};

export default Error;