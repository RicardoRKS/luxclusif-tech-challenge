package com.axide.farfetch.controller;

import com.axide.farfetch.command.CustomerBalanceDto;
import com.axide.farfetch.converters.CustomerBalanceDtoToCustomer;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.services.CustomerService;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class CustomerBalanceDtoToCustomerTest {

    private CustomerService customerService;
    private CustomerBalanceDtoToCustomer customerBalanceDtoToCustomer;


    @Before
    public void setup() {
        customerService = mock(CustomerService.class);

        customerBalanceDtoToCustomer = new CustomerBalanceDtoToCustomer();
        customerBalanceDtoToCustomer.setCustomerService(customerService);
    }

    @Test
    public void testConvert() {
        Long fakeCustomerId = 999L;
        String fakeCustomerBalance = "9.99";

        Customer fakeCustomer = spy(Customer.class);
        fakeCustomer.setId(fakeCustomerId);

        CustomerBalanceDto fakeCustomerBalanceDto = new CustomerBalanceDto();
        fakeCustomerBalanceDto.setId(fakeCustomerId);
        fakeCustomerBalanceDto.setBalance(fakeCustomerBalance);

        when(customerService.get(fakeCustomerId)).thenReturn(fakeCustomer);

        Customer customer = customerBalanceDtoToCustomer.convert(fakeCustomerBalanceDto);

        verify(customerService, times(1)).get(fakeCustomerId);
        assert customer != null;
        assertEquals(customer.getId(), fakeCustomerId);
        assertEquals(customer.getBalance(), Double.parseDouble(fakeCustomerBalance));

    }
}
