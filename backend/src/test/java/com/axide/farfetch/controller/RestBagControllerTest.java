package com.axide.farfetch.controller;

import com.axide.farfetch.command.BagDto;
import com.axide.farfetch.controllers.RestBagController;
import com.axide.farfetch.converters.BagDtoToBag;
import com.axide.farfetch.converters.BagToBagDto;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.services.BagService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestBagControllerTest {


    @Mock
    private BagService bagService;

    @Mock
    private BagDtoToBag bagDtoToBag;

    @Mock
    private BagToBagDto bagToBagDto;


    @InjectMocks
    private RestBagController restBagController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(restBagController).build();
        objectMapper = new ObjectMapper();
    }


    @Test
    public void testGetBag() throws Exception {

        Long fakeId = 999L;
        Long fakeCustomerId = 99L;
        String brand = "Gucci";
        String condition = "Pristine";
        String size = "Large";


        Bag bag = new Bag();
        bag.setId(fakeId);
        bag.setBrand(brand);
        bag.setBag_condition(condition);
        bag.setBag_size(size);


        BagDto bagDto = new BagDto();
        bagDto.setBagId(fakeId);
        bagDto.setBrand(brand);
        bagDto.setCondition(condition);
        bagDto.setSize(size);


        when(bagService.getBag(fakeId)).thenReturn(bag);
        when(bagToBagDto.convert(bag)).thenReturn(bagDto);

        mockMvc.perform(get("/api/bag/{id}", bag.getId()))
                .andExpect(jsonPath("$.brand").value(brand))
                .andExpect(jsonPath("$.condition").value(condition))
                .andExpect(jsonPath("$.size").value(size))
                .andExpect(status().isOk());

        verify(bagService, times(1)).getBag(fakeId);
        verify(bagToBagDto, times(1)).convert(bag);
    }
}
