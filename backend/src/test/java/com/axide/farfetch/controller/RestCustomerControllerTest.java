package com.axide.farfetch.controller;

import com.axide.farfetch.command.CustomerBalanceDto;
import com.axide.farfetch.command.CustomerDto;
import com.axide.farfetch.controllers.RestCustomerController;
import com.axide.farfetch.converters.CustomerDtoToCustomer;
import com.axide.farfetch.converters.CustomerToCustomerDto;
import com.axide.farfetch.exceptions.AssociationExistsException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.services.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class RestCustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @Mock
    private CustomerToCustomerDto customerToCustomerDto;

    @Mock
    private CustomerDtoToCustomer customerDtoToCustomer;


    @InjectMocks
    private RestCustomerController restCustomerController;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(restCustomerController).build();
        objectMapper = new ObjectMapper();

    }
    @Test
    public void testListCustomers() throws Exception {

        Long fakeId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "777888999";
        String email = "mail@gmail.com";

        Customer customer = new Customer();
        customer.setId(fakeId);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setEmail(email);

        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(fakeId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);

        List<Customer> customers = new ArrayList<>();
        customers.add(customer);


        when(customerService.list()).thenReturn(customers);
        when(customerToCustomerDto.convert(customer)).thenReturn(customerDto);

        mockMvc.perform(get("/api/customer/"))
                .andExpect(jsonPath("$[0].id").value(fakeId))
                .andExpect(jsonPath("$[0].firstName").value(firstName))
                .andExpect(jsonPath("$[0].lastName").value(lastName))
                .andExpect(jsonPath("$[0].email").value(email))
                .andExpect(jsonPath("$[0].phone").value(phone))
                .andExpect(status().isOk());

        verify(customerService, times(1)).list();
        verify(customerToCustomerDto, times(1)).convert(customer);
    }

    @Test
    public void testShowCustomer() throws Exception {

        Long fakeId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "777888999";
        String email = "mail@gmail.com";

        Customer customer = new Customer();
        customer.setId(fakeId);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setEmail(email);

        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(fakeId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);


        when(customerService.get(fakeId)).thenReturn(customer);
        when(customerToCustomerDto.convert(customer)).thenReturn(customerDto);

        mockMvc.perform(get("/api/customer/{id}", customer.getId()))
                .andExpect(jsonPath("$.id").value(fakeId))
                .andExpect(jsonPath("$.firstName").value(firstName))
                .andExpect(jsonPath("$.lastName").value(lastName))
                .andExpect(jsonPath("$.email").value(email))
                .andExpect(jsonPath("$.phone").value(phone))
                .andExpect(status().isOk());

        verify(customerService, times(1)).get(fakeId);
        verify(customerToCustomerDto, times(1)).convert(customer);
    }


    @Test
    public void testShowInvalidCustomer() throws Exception {

        Long invalidId = 888L;
        Long fakeId = 999L;
        Customer customer = new Customer();
        customer.setId(fakeId);


        when(customerService.get(invalidId)).thenReturn(null);

        mockMvc.perform(get("/api/customer/{id}", invalidId))
                .andExpect(status().isNotFound());

        verify(customerService, times(1)).get(invalidId);
    }

    @Test
    public void testAddCustomer() throws Exception {

        Long fakeId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "777888999";
        String email = "mail@gmail.com";


        CustomerDto customerDto = new CustomerDto();
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);

        Customer customer = new Customer();
        customer.setId(fakeId);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setEmail(email);



        when(customerDtoToCustomer.convert(ArgumentMatchers.any(CustomerDto.class))).thenReturn(customer);
        when(customerService.save(customer)).thenReturn(customer);

        mockMvc.perform(post("/api/customer/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerDto)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("http://localhost/api/customer/" + customer.getId())));

        //verify properties of bound command object
        ArgumentCaptor<CustomerDto> boundCustomer = ArgumentCaptor.forClass(CustomerDto.class);

        verify(customerDtoToCustomer, times(1)).convert(boundCustomer.capture());
        verify(customerService, times(1)).save(customer);

        assertNull(boundCustomer.getValue().getId());
        assertEquals(firstName, boundCustomer.getValue().getFirstName());
        assertEquals(lastName, boundCustomer.getValue().getLastName());
        assertEquals(email, boundCustomer.getValue().getEmail());
        assertEquals(phone, boundCustomer.getValue().getPhone());
    }

    @Test
    public void testAddCustomerWithBadRequest() throws Exception {

        mockMvc.perform(post("/api/customer/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testEditCustomer() throws Exception {

        Long fakeId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "999888999";
        String email = "mail@gmail.com";


        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(fakeId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);


        Customer customer = new Customer();
        customer.setId(fakeId);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setEmail(email);


        when(customerDtoToCustomer.convert(ArgumentMatchers.any(CustomerDto.class))).thenReturn(customer);
        when(customerService.save(customer)).thenReturn(customer);
        when(customerService.get(fakeId)).thenReturn(customer);

        mockMvc.perform(put("/api/customer/{id}", fakeId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerDto)))
                .andExpect(status().isOk());

        ArgumentCaptor<CustomerDto> boundCustomer = ArgumentCaptor.forClass(CustomerDto.class);

        verify(customerDtoToCustomer, times(1)).convert(boundCustomer.capture());
        verify(customerService, times(1)).save(customer);

        assertEquals(fakeId, boundCustomer.getValue().getId());
        assertEquals(firstName, boundCustomer.getValue().getFirstName());
        assertEquals(lastName, boundCustomer.getValue().getLastName());
        assertEquals(email, boundCustomer.getValue().getEmail());
        assertEquals(phone, boundCustomer.getValue().getPhone());
    }

    @Test
    public void testEditCustomerWithBadRequest() throws Exception {

        Long invalidId = 199L;
        Long fakeId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "999888999";
        String email = "mail@gmail.com";


        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(fakeId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);



        mockMvc.perform(put("/api/customer/{id}", invalidId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testEditInvalidCustomer() throws Exception {

        Long invalidId = 999L;
        String firstName = "Ricardo";
        String lastName = "Simões";
        String phone = "999888999";
        String email = "mail@gmail.com";


        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(invalidId);
        customerDto.setFirstName(firstName);
        customerDto.setLastName(lastName);
        customerDto.setPhone(phone);
        customerDto.setEmail(email);


        Customer customer = new Customer();
        customer.setId(invalidId);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setPhone(phone);
        customer.setEmail(email);



        when(customerDtoToCustomer.convert(ArgumentMatchers.any(CustomerDto.class))).thenReturn(customer);
        when(customerService.save(customer)).thenReturn(customer);
        when(customerService.get(invalidId)).thenReturn(null);

        mockMvc.perform(put("/api/customer/{id}", invalidId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteCustomer() throws Exception {

        Long fakeId = 999L;

        mockMvc.perform(delete("/api/customer/{id}", fakeId))
                .andExpect(status().isNoContent());

        verify(customerService, times(1)).delete(fakeId);
    }

    @Test
    public void testDeleteInvalidCustomer() throws Exception {

        Long invalidId = 888L;

        doThrow(new CustomerNotFoundException()).when(customerService).delete(ArgumentMatchers.any(Long.class));

        mockMvc.perform(delete("/api/customer/{id}", invalidId))
                .andExpect(status().isNotFound());

        verify(customerService, times(1)).delete(invalidId);
    }

    @Test
    public void testDeleteCustomerWithOpenAccount() throws Exception {

        Long fakeId = 999L;

        doThrow(new AssociationExistsException()).when(customerService).delete(ArgumentMatchers.any(Long.class));

        mockMvc.perform(delete("/api/customer/{id}", fakeId))
                .andExpect(status().isBadRequest());

        verify(customerService, times(1)).delete(fakeId);
    }

    @Test
    public void testDeposit() throws Exception {

        Long fakeCustomerId = 999L;
        String amount = "777";

        CustomerBalanceDto customerBalanceDto = new CustomerBalanceDto();
        customerBalanceDto.setId(fakeCustomerId);
        customerBalanceDto.setBalance(amount);

        mockMvc.perform(put("/api/customer/{cid}/deposit", fakeCustomerId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerBalanceDto)))
                .andExpect(status().isOk());

        verify(customerService, times(1)).deposit(fakeCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));

    }

    @Test
    public void testDepositWithBadRequest() throws Exception {

        Long fakeCustomerId = 999L;

        mockMvc.perform(put("/api/customer/{cid}/deposit", fakeCustomerId))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDepositToInvalidCustomer() throws Exception {

        Long invalidCustomerId = 777L;
        String amount = "777";

        CustomerBalanceDto customerBalanceDto = new CustomerBalanceDto();
        customerBalanceDto.setId(invalidCustomerId);
        customerBalanceDto.setBalance(amount);


        doThrow(new CustomerNotFoundException()).when(customerService).deposit(invalidCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));

        mockMvc.perform(put("/api/customer/{cid}/deposit", invalidCustomerId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerBalanceDto)))
                .andExpect(status().isNotFound());

        verify(customerService, times(1)).deposit(invalidCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));
    }

    @Test
    public void testWithdraw() throws Exception {

        Long fakeCustomerId = 999L;
        String amount = "777";

        CustomerBalanceDto customerBalanceDto = new CustomerBalanceDto();
        customerBalanceDto.setId(fakeCustomerId);
        customerBalanceDto.setBalance(amount);

        mockMvc.perform(put("/api/customer/{cid}/deposit", fakeCustomerId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerBalanceDto)))
                .andExpect(status().isOk());

        verify(customerService, times(1)).withdraw(fakeCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));
    }

    @Test
    public void testWithdrawWithBadRequest() throws Exception {

        Long fakeCustomerId = 999L;

        mockMvc.perform(put("/api/customer/{cid}/withdraw", fakeCustomerId))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testWithdrawToInvalidCustomer() throws Exception {

        Long invalidCustomerId = 777L;
        String amount = "777";

        CustomerBalanceDto customerBalanceDto = new CustomerBalanceDto();
        customerBalanceDto.setId(invalidCustomerId);
        customerBalanceDto.setBalance(amount);


        doThrow(new CustomerNotFoundException()).when(customerService).withdraw(invalidCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));

        mockMvc.perform(put("/api/customer/{cid}/withdraw", invalidCustomerId)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(customerBalanceDto)))
                .andExpect(status().isNotFound());

        verify(customerService, times(1)).withdraw(invalidCustomerId, Double.parseDouble(customerBalanceDto.getBalance()));
    }








}
