package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.model.bags.Extras;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ExtrasRepositoryDaoTest {
    private ExtrasRepositoryDao extrasRepositoryDao;
    private EntityManager em;

    @Before
    public void setup() {

        em = mock(EntityManager.class);

        extrasRepositoryDao = new ExtrasRepositoryDao();
        extrasRepositoryDao.setEm(em);

    }

    @Test
    public void testFindAll() {

        // setup
        List<Extras> mockExtrasList = new ArrayList<>();
        CriteriaQuery criteriaQuery = mock(CriteriaQuery.class);
        CriteriaBuilder criteriaBuilder = mock(CriteriaBuilder.class);
        TypedQuery typedQuery = mock(TypedQuery.class);
        when(em.getCriteriaBuilder()).thenReturn(criteriaBuilder);
        when(criteriaBuilder.createQuery(Extras.class)).thenReturn(criteriaQuery);
        when(em.createQuery(criteriaQuery)).thenReturn(typedQuery);
        when(em.createQuery(anyString(), any(Class.class))).thenReturn(typedQuery);
        when(em.createQuery(any(CriteriaQuery.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(mockExtrasList);

        // exercise
        List<Extras> extrasList = extrasRepositoryDao.findAll();

        // verify
        verify(typedQuery, times(1)).getResultList();
        assertEquals(mockExtrasList, extrasList);
    }

    @Test
    public void testFindById() {

        // setup
        Long fakeId = 9999L;
        Extras fakeExtras = new Extras();
        fakeExtras.setId(fakeId);
        when(em.find(Extras.class, fakeId)).thenReturn(fakeExtras);

        // exercise
        Extras extras = extrasRepositoryDao.findById(fakeId);

        // verify
        verify(em, times(1)).find(Extras.class, fakeId);
        assertEquals(fakeExtras, extras);

    }

    @Test
    public void testSaveOrUpdate() {

        // setup
        Extras fakeExtras = new Extras();
        when(em.merge(any(Extras.class))).thenReturn(fakeExtras);

        // exercise
        Extras extras = extrasRepositoryDao.save(fakeExtras);

        // verify
        verify(em, times(1)).merge(any(Extras.class));
        assertEquals(fakeExtras, extras);

    }

    @Test
    public void testDelete() {

        // setup
        Long fakeId = 9999L;
        Extras fakeExtras = new Extras();
        fakeExtras.setId(fakeId);
        when(em.find(Extras.class, fakeId)).thenReturn(fakeExtras);

        // exercise
        extrasRepositoryDao.delete(fakeId);

        // verify
        verify(em, times(1)).remove(fakeExtras);

    }
}
