package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.model.bags.Bag;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;


public class BagRepositoryDaoTest {

    private BagRepositoryDao bagRepositoryDao;
    private EntityManager em;

    @Before
    public void setup() {
        em = mock(EntityManager.class);

        bagRepositoryDao = new BagRepositoryDao();
        bagRepositoryDao.setEm(em);
    }

    @Test
    public void testFindAll(){


        List<Bag> mockBags = new ArrayList<>();
        CriteriaQuery criteriaQuery = mock(CriteriaQuery.class);
        CriteriaBuilder criteriaBuilder = mock(CriteriaBuilder.class);
        TypedQuery typedQuery = mock(TypedQuery.class);
        when(em.getCriteriaBuilder()).thenReturn(criteriaBuilder);
        when(criteriaBuilder.createQuery(Bag.class)).thenReturn(criteriaQuery);
        when(em.createQuery(criteriaQuery)).thenReturn(typedQuery);
        when(em.createQuery(anyString(), any(Class.class))).thenReturn(typedQuery);
        when(em.createQuery(any(CriteriaQuery.class))).thenReturn(typedQuery);
        when(typedQuery.getResultList()).thenReturn(mockBags);


        List<Bag> customersBags = bagRepositoryDao.findAll();


        verify(typedQuery, times(1)).getResultList();
        assertEquals(mockBags, customersBags);
    }

    @Test
    public void testFindById() {
        Long fakeId = 999L;
        Bag fakeBag = new Bag();
        fakeBag.setId(fakeId);
        when(em.find(Bag.class, fakeId)).thenReturn(fakeBag);

        Bag customerBag = bagRepositoryDao.findById(fakeId);

        verify(em,times(1)).find(Bag.class,fakeId);
        assertEquals(fakeBag,customerBag);
    }

    @Test
    public void testSave(){
        Bag fakeBag = new Bag();
        when(em.merge(any(Bag.class))).thenReturn(fakeBag);

        Bag customerBag = bagRepositoryDao.save(fakeBag);

        verify(em, times(1)).merge(any(Bag.class));
        assertEquals(fakeBag, customerBag);
    }

    @Test
    public void testDelete(){
        Long fakeId = 999L;
        Bag fakeBag = new Bag();
        fakeBag.setId(fakeId);
        when(em.find(Bag.class, fakeId)).thenReturn(fakeBag);

        bagRepositoryDao.delete(fakeId);

        verify(em, times(1)).remove(fakeBag);
    }

}
