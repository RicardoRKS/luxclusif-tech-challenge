package com.axide.farfetch.converters;


import com.axide.farfetch.command.CustomerDto;
import com.axide.farfetch.model.Customer;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.*;

public class CustomerToCustomerDtoTest {

    private CustomerToCustomerDto customerToCustomerDto;

    @Before
    public void setup() {
        customerToCustomerDto = new CustomerToCustomerDto();
    }

    @Test
    public void testConvert() {


        Long fakeCustomerId = 999L;
        String fakeFirstName = "FakeBoy";
        String fakeLastName = "McFake";
        String fakeEmail = "fake@fakegmail.com";
        String fakePhone = "912345678";

        Customer fakeCustomer = spy(Customer.class);
        fakeCustomer.setId(fakeCustomerId);
        fakeCustomer.setFirstName(fakeFirstName);
        fakeCustomer.setLastName(fakeLastName);
        fakeCustomer.setEmail(fakeEmail);
        fakeCustomer.setPhone(fakePhone);


        CustomerDto customerDto = customerToCustomerDto.convert(fakeCustomer);


        verify(fakeCustomer, times(1)).getId();
        verify(fakeCustomer, times(1)).getFirstName();
        verify(fakeCustomer, times(1)).getLastName();
        verify(fakeCustomer, times(1)).getEmail();
        verify(fakeCustomer, times(1)).getPhone();

        assert customerDto != null;
        assertEquals(customerDto.getId(), fakeCustomerId);
        assertEquals(customerDto.getFirstName(), fakeFirstName);
        assertEquals(customerDto.getLastName(), fakeLastName);
        assertEquals(customerDto.getEmail(), fakeEmail);
        assertEquals(customerDto.getPhone(), fakePhone);
    }
}
