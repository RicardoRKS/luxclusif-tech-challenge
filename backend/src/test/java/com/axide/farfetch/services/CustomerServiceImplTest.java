package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.*;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.repositories.CustomerDao;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CustomerServiceImplTest {

    private static final double DOUBLE_PRECISION = 0.1;

    private CustomerDao customerDao;
    private CustomerServiceImpl customerService;

    @Before
    public void setup() {
        customerDao = mock(CustomerDao.class);

        customerService = new CustomerServiceImpl();
        customerService.setCustomerDao(customerDao);
    }

    @Test
    public void testGet() {
        Long fakeId = 999L;
        Customer fakeCustomer = new Customer();
        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);

        Customer customer = customerService.get(fakeId);

        assertEquals(fakeCustomer, customer);
    }

    @Test
    public void testGetByEmail() {
        String fakeEmail = "mrricardorks@gmail.com";
        Customer fakeCustomer = new Customer();
        when(customerDao.findByEmail(fakeEmail)).thenReturn(fakeCustomer);

        Customer customer = customerService.getByEmail(fakeEmail);

        assertEquals(fakeCustomer, customer);
    }

    @Test
    public void testGetCredit() {
        Long fakeId = 999L;
        double fakeCredit = 100.50;
        double fakeExpense = 50;
        Customer fakeCustomer = new Customer();
        fakeCustomer.addCredit(fakeCredit);
        fakeCustomer.removeCredit(fakeExpense);
        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);

        double result = customerService.getCredit(fakeId);

        assertEquals(fakeCredit - fakeExpense, result, DOUBLE_PRECISION);
    }

    @Test(expected = NotEnoughBalanceException.class)
    public void testNotEnoughBalance() throws SecondLifeException {
        Long fakeId = 999L;
        double fakeCredit = 100.50;
        Customer fakeCustomer = new Customer();
        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);

        customerService.withdraw(fakeId, fakeCredit);
    }

    @Test(expected = CustomerNotFoundException.class)
    public void testInvalidCustomerAddCredit() throws SecondLifeException {
        when(customerDao.findById(anyLong())).thenReturn(null);

        customerService.deposit(5L, 100.0);
    }

    @Test(expected = CustomerNotFoundException.class)
    public void testInvalidCustomerRemoveCredit() throws SecondLifeException {
        when(customerDao.findById(anyLong())).thenReturn(null);

        customerService.withdraw(5L, 100.0);
    }

    @Test
    public void testDelete() throws SecondLifeException {

        //setup
        Customer fakeCustomer = new Customer();
        Long fakeId = 999L;
        fakeCustomer.setId(fakeId);

        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);

        //exercise
        customerService.delete(fakeId);

        //verify
        verify(customerDao, times(1)).delete(fakeId);
    }

    @Test(expected = AssociationExistsException.class)
    public void testDeleteCustomerWithBags() throws SecondLifeException {

        //setup
        Bag fakeBag = new Bag();
        Customer fakeCustomer = new Customer();
        Long fakeId = 999L;
        fakeCustomer.setId(fakeId);
        fakeCustomer.addToBagsList(fakeBag);

        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);

        //exercise
        customerService.delete(fakeId);

        //verify
        assertFalse(fakeCustomer.getBagsList().isEmpty());
        verify(customerDao, times(1)).delete(fakeId);
    }




}
