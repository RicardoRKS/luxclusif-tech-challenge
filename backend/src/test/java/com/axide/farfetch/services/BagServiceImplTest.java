package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.BagNotFoundException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.repositories.BagDao;
import com.axide.farfetch.repositories.CustomerDao;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class BagServiceImplTest {

    private BagDao bagDao;
    private CustomerDao customerDao;
    private BagServiceImpl bagService;

    @Before
    public void setup(){
        bagDao = mock(BagDao.class);
        customerDao = mock(CustomerDao.class);

        bagService = new BagServiceImpl();
        bagService.setBagDao(bagDao);
        bagService.setCustomerDao(customerDao);
    }

    @Test
    public void testAddBag() throws CustomerNotFoundException {
        Long fakeId = 999L;
        Customer fakeCustomer = new Customer();

        Bag fakeBag = new Bag();

        when(customerDao.findById(fakeId)).thenReturn(fakeCustomer);
        when(bagDao.findById(fakeBag.getId())).thenReturn(fakeBag);

        bagService.save(fakeId, fakeBag);

        assertTrue(customerDao.findById(fakeId).getBagsList().contains(fakeBag));
    }

    @Test(expected = CustomerNotFoundException.class)
    public void testAddBagCustomerNotFound() throws CustomerNotFoundException {
        Bag fakeBag = new Bag();
        when(bagDao.findById(fakeBag.getId())).thenReturn(fakeBag);
        when(customerDao.findById(anyLong())).thenReturn(null);

        bagService.save(1L, fakeBag);
    }

    @Test
    public void testDeleteBag() throws CustomerNotFoundException, BagNotFoundException {
        Long fakeCustomerId = 999L;
        Long fakeBagId = 100L;
        Customer fakeCustomer = spy(new Customer());
        Bag fakeBag = new Bag();

        fakeBag.setId(fakeBagId);
        fakeBag.setCustomer(fakeCustomer);
        fakeCustomer.addToBagsList(fakeBag);

        when(customerDao.findById(fakeCustomerId)).thenReturn(fakeCustomer);
        when(fakeCustomer.getId()).thenReturn(fakeCustomerId);
        when(bagDao.findById(fakeBagId)).thenReturn(fakeBag);

        bagService.delete(fakeCustomerId, fakeBagId);

        assertFalse(customerDao.findById(fakeCustomerId).getBagsList().contains(fakeBag));
    }

    @Test(expected = BagNotFoundException.class)
    public void testDeleteBagNotFound() throws CustomerNotFoundException, BagNotFoundException {
        Long fakeCustomerId = 999L;
        Long fakeBagId = 100L;
        Customer fakeCustomer = new Customer();
        Bag fakeBag = spy(new Bag());

        fakeCustomer.setId(fakeCustomerId);
        fakeBag.setId(fakeBagId);
        fakeCustomer.addToBagsList(fakeBag);

        when(customerDao.findById(fakeCustomerId)).thenReturn(fakeCustomer);
        when(bagDao.findById(anyLong())).thenReturn(null);


        bagService.delete(fakeCustomerId, fakeBag.getId());
    }
}
