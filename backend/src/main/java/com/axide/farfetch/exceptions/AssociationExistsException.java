package com.axide.farfetch.exceptions;

import com.axide.farfetch.errors.ErrorMessage;

public class AssociationExistsException extends SecondLifeException {

    public AssociationExistsException() {
        super(ErrorMessage.ASSOCIATION_EXISTS);
    }
}
