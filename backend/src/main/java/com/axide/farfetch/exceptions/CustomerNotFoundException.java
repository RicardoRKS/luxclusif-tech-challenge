package com.axide.farfetch.exceptions;

import com.axide.farfetch.errors.ErrorMessage;

public class CustomerNotFoundException extends SecondLifeException {

    public CustomerNotFoundException() {
        super(ErrorMessage.CUSTOMER_NOT_FOUND);
    }
}
