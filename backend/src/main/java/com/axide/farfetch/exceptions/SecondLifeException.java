package com.axide.farfetch.exceptions;

public class SecondLifeException extends Exception {

    public SecondLifeException(String message) {
        super(message);
    }
}
