package com.axide.farfetch.exceptions;

import com.axide.farfetch.errors.ErrorMessage;

public class BagNotFoundException extends SecondLifeException {

    public BagNotFoundException() {
        super(ErrorMessage.BAG_NOT_FOUND);
    }
}
