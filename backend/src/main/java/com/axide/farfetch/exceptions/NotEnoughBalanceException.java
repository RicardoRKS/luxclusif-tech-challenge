package com.axide.farfetch.exceptions;

import com.axide.farfetch.errors.ErrorMessage;

public class NotEnoughBalanceException extends SecondLifeException {
    public NotEnoughBalanceException() {
        super(ErrorMessage.NOT_ENOUGH_BALANCE);
    }
}
