package com.axide.farfetch.model;

/**
 * Interface for a model, provides methods to get, set ids and enforces this behaviour across all model components.
 */
public interface Model {

    Long getId();

    void setId(Long id);
}
