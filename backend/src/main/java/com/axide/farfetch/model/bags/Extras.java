package com.axide.farfetch.model.bags;

import com.axide.farfetch.model.Model;

import javax.persistence.*;

/*I've considered 3 options on how to deal with Extras.
1. Simply creating the fields in the bag object -> Rejected, since it makes the Bag object too overburdened, in my opinion.
2. Embedding the Extras object in the Table -> Rejected, it makes the visualization of the tables less efficient and causes conflicts with persistence
3. Creating a separate table -> Easier view, less complex objects.
 */
@Entity
@Table(name = "extras")
public class Extras implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;


    @ManyToOne
    private Bag bag;

    private int box;
    private int authenticityCard;
    private int shoulderStrap;
    private int dustbag;
    private int pouch;
    private int padlockAndKey;
    private int bagcharm;
    private int nameTag;
    private int mirror;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }


    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public int getBox() {
        return box;
    }

    public void setBox(int box) {
        this.box = box;
    }

    public int getAuthenticityCard() {
        return authenticityCard;
    }

    public void setAuthenticityCard(int authenticity_card) {
        this.authenticityCard = authenticity_card;
    }

    public int getShoulderStrap() {
        return shoulderStrap;
    }

    public void setShoulderStrap(int shoulder_strap) {
        this.shoulderStrap = shoulder_strap;
    }

    public int getDustbag() {
        return dustbag;
    }

    public void setDustbag(int dustbag) {
        this.dustbag = dustbag;
    }

    public int getPouch() {
        return pouch;
    }

    public void setPouch(int pouch) {
        this.pouch = pouch;
    }

    public int getPadlockAndKey() {
        return padlockAndKey;
    }

    public void setPadlockAndKey(int padlock_and_key) {
        this.padlockAndKey = padlock_and_key;
    }

    public int getBagcharm() {
        return bagcharm;
    }

    public void setBagcharm(int bagcharm) {
        this.bagcharm = bagcharm;
    }

    public int getNameTag() {
        return nameTag;
    }

    public void setNameTag(int nameTag) {
        this.nameTag = nameTag;
    }

    public int getMirror() {
        return mirror;
    }

    public void setMirror(int mirror) {
        this.mirror = mirror;
    }
}
