package com.axide.farfetch.model.bags;

import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bag")
public class Bag implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(
            cascade = {CascadeType.ALL},

            orphanRemoval = true,

            mappedBy = "bag",

            fetch = FetchType.EAGER
    )
    private final List<Extras> extrasList = new ArrayList<>();


    @Version
    private Integer version;
    private String brand;
    private String bag_condition;
    private String bag_size;


    @ManyToOne
    private Customer customer;


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBag_condition() {
        return bag_condition;
    }

    public void setBag_condition(String bag_condition) {
        this.bag_condition = bag_condition;
    }

    public String getBag_size() {
        return bag_size;
    }

    public void setBag_size(String size) {
        this.bag_size = size;
    }

    public List<Extras> getExtrasList() {
        return extrasList;
    }

    public void setExtrasList(Extras extras) {
        this.extrasList.add(extras);
        extras.setBag(this);
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
