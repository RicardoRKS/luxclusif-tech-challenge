package com.axide.farfetch.model;

import com.axide.farfetch.model.bags.Bag;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer implements Model {

    @OneToMany(
            // propagate changes on customer entity to account entities
            cascade = {CascadeType.ALL},

            // make sure to remove bags still on sale if unlinked from customer
            orphanRemoval = true,

            // user customer foreign key on account table to establish
            // the many-to-one relationship instead of a join table
            mappedBy = "customer",

            // fetch bags from database together with user
            // I don't know how many second-handed bags a customer usually wants to sell, so I'm being optimistic and using an eager fetch type instead of a lazy one.
            fetch = FetchType.EAGER
    )
    //Array List since most of the operation will revolve around fetch
    private final List<Bag> bagsList = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Version
    private Integer version;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Double balance = 0.0;

    public void addToBagsList(Bag bag) {
        this.bagsList.add(bag);
        bag.setCustomer(this);
    }

    public void removeBag(Bag bag) {
        bagsList.remove(bag);
        bag.setCustomer(null);
    }

    public void addCredit(Double balance) {
        this.balance += balance;
    }

    public boolean removeCredit(Double balance) {
        if (this.balance >= balance) {
            this.balance -= balance;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Bag> getBagsList() {
        return bagsList;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
