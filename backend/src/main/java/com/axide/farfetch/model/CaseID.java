package com.axide.farfetch.model;

import javax.persistence.*;

@Entity
@Table(name = "caseID")
public class CaseID implements Model {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    private Long customerId;

    private Long bagId;

    private Long extrasId;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long case_id) {
        this.id = case_id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getBagId() {
        return bagId;
    }

    public void setBagId(Long bagId) {
        this.bagId = bagId;
    }

    public Long getExtrasId() {
        return extrasId;
    }

    public void setExtrasId(Long extrasId) {
        this.extrasId = extrasId;
    }
}
