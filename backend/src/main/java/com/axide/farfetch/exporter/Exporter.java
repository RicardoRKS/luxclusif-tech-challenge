package com.axide.farfetch.exporter;

public interface Exporter<T> {

    void authenticate();

    void export(T model, Long id) throws Exception;

}
