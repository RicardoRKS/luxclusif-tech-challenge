package com.axide.farfetch.exporter.spreadsheet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;

//Local exporter to csv

@Component
public class ExcelExporter {

    public static void main(String[] args) {
        ExcelExporter excelExporter = new ExcelExporter();

        excelExporter.export("customer");
        excelExporter.export("bag");
        excelExporter.export("extras");
    }

    public void export(String table) {
        String jdbcURL = "jdbc:h2:mem:secondlife";
        String username = "sa";
        String password = "";

        String excelFilePath = "excelExports/" + table.concat("_Export.csv");

        try (Connection connection = DriverManager.getConnection(jdbcURL, username, password)) {
            String sql = "SELECT * FROM ".concat(table);

            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(sql);

            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(table);

            writeHeaderLine(result, sheet);

            writeDataLines(result, sheet);

            FileOutputStream outputStream = new FileOutputStream(excelFilePath);
            workbook.write(outputStream);
            workbook.close();

            statement.close();

        } catch (SQLException e) {

            System.out.println("Datababse error:");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("File IO error:");
            e.printStackTrace();
        }
    }

    private void writeHeaderLine(ResultSet result, XSSFSheet sheet) throws SQLException {

        ResultSetMetaData metaData = result.getMetaData();
        int numberOfColumns = metaData.getColumnCount();

        Row headerRow = sheet.createRow(0);


        for (int i = 1; i <= numberOfColumns; i++) {
            String columnName = metaData.getColumnName(i);
            Cell headerCell = headerRow.createCell(i - 1);
            headerCell.setCellValue(columnName);
        }
    }

    private void writeDataLines(ResultSet result, XSSFSheet sheet)
            throws SQLException {
        ResultSetMetaData metaData = result.getMetaData();
        int numberOfColumns = metaData.getColumnCount();

        int rowCount = 1;

        while (result.next()) {
            Row row = sheet.createRow(rowCount++);

            for (int i = 1; i <= numberOfColumns; i++) {
                Object valueObject = result.getObject(i);

                Cell cell = row.createCell(i - 1);

                if (valueObject instanceof Boolean) {
                    cell.setCellValue((Boolean) valueObject);
                } else if (valueObject instanceof Double) {
                    cell.setCellValue((double) valueObject);
                } else if (valueObject instanceof Integer) {
                    cell.setCellValue((int) valueObject);
                } else if (valueObject instanceof Long) {
                    cell.setCellValue((long) valueObject);
                } else {
                    cell.setCellValue((String) valueObject);
                }
            }
        }
    }
}
