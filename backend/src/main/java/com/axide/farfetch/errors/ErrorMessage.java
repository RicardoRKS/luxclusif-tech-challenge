package com.axide.farfetch.errors;

public class ErrorMessage {
    public static final String CUSTOMER_NOT_FOUND = "Customer does not exist";
    public static final String BAG_NOT_FOUND = "Bag does not exist";
    public static final String ASSOCIATION_EXISTS = "Entity contains association with another entity";
    public static final String NOT_ENOUGH_BALANCE = "Not enough balance";
}
