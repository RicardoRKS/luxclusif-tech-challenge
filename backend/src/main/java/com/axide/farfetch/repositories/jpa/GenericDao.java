package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.model.Model;
import com.axide.farfetch.repositories.Dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


public abstract class GenericDao<T extends Model> implements Dao<T> {

    protected final Class<T> modelType;

    @PersistenceContext
    protected EntityManager em;


    public GenericDao(Class<T> modelType) {
        this.modelType = modelType;
    }


    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<T> findAll() {

        CriteriaQuery<T> criteriaQuery = em.getCriteriaBuilder().createQuery(modelType);
        Root<T> root = criteriaQuery.from(modelType);
        return em.createQuery(criteriaQuery).getResultList();

    }


    @Override
    public T findById(Long id) {
        return em.find(modelType, id);
    }


    @Override
    public T save(T modelObject) {
        return em.merge(modelObject);
    }


    @Override
    public void delete(Long id) {
        em.remove(em.find(modelType, id));
    }
}
