package com.axide.farfetch.repositories;

import com.axide.farfetch.model.bags.Extras;

public interface ExtrasDao extends Dao<Extras> {
}
