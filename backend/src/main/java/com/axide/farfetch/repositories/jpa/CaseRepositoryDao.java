package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.command.CaseDetailsDto;
import com.axide.farfetch.model.CaseID;
import com.axide.farfetch.repositories.CaseDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class CaseRepositoryDao extends GenericDao<CaseID> implements CaseDao {

    public CaseRepositoryDao() {
        super(CaseID.class);
    }

    @Override
    public CaseDetailsDto getCaseDetails(Long id) {
        String query = "SELECT c.firstName, c.lastName, c.email, b.brand, b.bag_condition, b.bag_size, e.box, e.authenticityCard, e.shoulderStrap, e.dustbag, e.pouch, e.padlockAndKey, e.bagcharm, e.nameTag, e.mirror FROM Customer c, Bag b, Extras e, CaseID cid WHERE cid.id = :case_id AND cid.bagId = b.id AND cid.customerId = c.id AND cid.extrasId = e.id";

        CaseDetailsDto caseDetailsDto = new CaseDetailsDto();
        System.out.println(id);
        try {
            List<Object[]> object = em.createQuery(query, Object[].class)
                    .setParameter("case_id", id)
                    .getResultList();

            for (Object[] row : object) {
                System.out.println(row.length);
                caseDetailsDto.setFirstName((String) row[0]);
                caseDetailsDto.setLastName((String) row[1]);
                caseDetailsDto.setEmail((String) row[2]);
                caseDetailsDto.setBrand((String) row[3]);
                caseDetailsDto.setCondition((String) row[4]);
                caseDetailsDto.setSize((String) row[5]);
                caseDetailsDto.setBox((int) row[6]);
                caseDetailsDto.setAuthenticityCard((int) row[7]);
                caseDetailsDto.setShoulderStrap((int) row[8]);
                caseDetailsDto.setDustbag((int) row[9]);
                caseDetailsDto.setPouch((int) row[10]);
                caseDetailsDto.setPadlockAndKey((int) row[11]);
                caseDetailsDto.setBagcharm((int) row[12]);
                caseDetailsDto.setNameTag((int) row[13]);
                caseDetailsDto.setMirror((int) row[14]);
            }

            System.out.println(caseDetailsDto.toString());
        } catch (NoResultException e) {
            return null;
        }
        return caseDetailsDto;
    }
}
