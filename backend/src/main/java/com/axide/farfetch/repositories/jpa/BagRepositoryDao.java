package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.repositories.BagDao;
import org.springframework.stereotype.Repository;

@Repository
public class BagRepositoryDao extends GenericDao<Bag> implements BagDao {

    public BagRepositoryDao() {
        super(Bag.class);
    }
}
