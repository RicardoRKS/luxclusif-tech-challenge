package com.axide.farfetch.repositories;

import com.axide.farfetch.model.Model;

import java.util.List;

public interface Dao<T extends Model> {

    List<T> findAll();

    T findById(Long id);

    T save(T modelObject);

    void delete(Long id);
}
