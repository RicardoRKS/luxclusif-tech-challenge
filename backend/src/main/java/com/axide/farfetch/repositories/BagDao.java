package com.axide.farfetch.repositories;

import com.axide.farfetch.model.bags.Bag;

public interface BagDao extends Dao<Bag> {
}
