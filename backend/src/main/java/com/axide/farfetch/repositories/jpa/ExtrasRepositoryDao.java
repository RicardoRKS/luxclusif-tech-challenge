package com.axide.farfetch.repositories.jpa;

import com.axide.farfetch.model.bags.Extras;
import com.axide.farfetch.repositories.ExtrasDao;
import org.springframework.stereotype.Repository;

@Repository
public class ExtrasRepositoryDao extends GenericDao<Extras> implements ExtrasDao {

    public ExtrasRepositoryDao() {
        super(Extras.class);
    }
}
