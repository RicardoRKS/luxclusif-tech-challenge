package com.axide.farfetch.repositories;

import com.axide.farfetch.model.Customer;

public interface CustomerDao extends Dao<Customer> {

    Customer findByEmail(String email);
}
