package com.axide.farfetch.repositories;

import com.axide.farfetch.command.CaseDetailsDto;
import com.axide.farfetch.model.CaseID;

public interface CaseDao extends Dao<CaseID> {

    CaseDetailsDto getCaseDetails(Long id);
}
