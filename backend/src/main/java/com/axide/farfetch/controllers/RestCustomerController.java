package com.axide.farfetch.controllers;

import com.axide.farfetch.command.CustomerBalanceDto;
import com.axide.farfetch.command.CustomerDto;
import com.axide.farfetch.command.EmailDto;
import com.axide.farfetch.converters.CustomerDtoToCustomer;
import com.axide.farfetch.converters.CustomerToCustomerBalanceDto;
import com.axide.farfetch.converters.CustomerToCustomerDto;
import com.axide.farfetch.exceptions.AssociationExistsException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.exceptions.NotEnoughBalanceException;
import com.axide.farfetch.exporter.spreadsheet.SheetExporter;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/customer")
public class RestCustomerController {

    private CustomerService customerService;
    private CustomerDtoToCustomer customerDtoToCustomer;
    private CustomerToCustomerDto customerToCustomerDto;
    private CustomerToCustomerBalanceDto customerToCustomerBalanceDto;
    private SheetExporter sheetExporter;


    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setCustomerDtoToCustomer(CustomerDtoToCustomer customerDtoToCustomer) {
        this.customerDtoToCustomer = customerDtoToCustomer;
    }

    @Autowired
    public void setCustomerToCustomerDto(CustomerToCustomerDto customerToCustomerDto) {
        this.customerToCustomerDto = customerToCustomerDto;
    }


    @Autowired
    public void setCustomerToCustomerBalanceDto(CustomerToCustomerBalanceDto customerToCustomerBalanceDto) {
        this.customerToCustomerBalanceDto = customerToCustomerBalanceDto;
    }

    @Autowired
    public void setSheetExporter(SheetExporter sheetExporter) {
        this.sheetExporter = sheetExporter;
    }

    @GetMapping(path = {"/", ""})
    public ResponseEntity<List<CustomerDto>> listCustomers() {

        List<CustomerDto> customerDtos = customerService.list()
                .stream()
                .map(customer -> customerToCustomerDto.convert(customer))
                .collect(Collectors.toList());

        return new ResponseEntity<>(customerDtos, HttpStatus.OK);
    }


    @PostMapping(path = "/login")
    public ResponseEntity<?> loginCustomer(@RequestBody EmailDto emailDto) {

        String email = emailDto.getEmail();
        Customer customer = customerService.getByEmail(email);

        if (customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CustomerDto> showCustomer(@PathVariable Long id) {

        Customer customer = customerService.get(id);

        if (customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.OK);
    }

    @PostMapping(path = {"/", ""})
    public ResponseEntity<?> addCustomer(@Valid @RequestBody CustomerDto customerDto, BindingResult bindingResult) throws Exception {

        Customer customer;

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (customerService.getByEmail(customerDto.getEmail()) == null) {

            customer = customerService.save(customerDtoToCustomer.convert(customerDto));

            sheetExporter.export("customer", 0L);

            return new ResponseEntity<>(customerToCustomerDto.convert(customer), HttpStatus.CREATED);
        }


        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<CustomerDto> editCustomer(@Valid @RequestBody CustomerDto customerDto, BindingResult bindingResult, @PathVariable Long id) {

        if (bindingResult.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (customerDto.getId() != null && !customerDto.getId().equals(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (customerService.get(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        customerDto.setId(id);

        customerService.save(customerDtoToCustomer.convert(customerDto));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<CustomerDto> deleteCustomer(@PathVariable Long id) {

        try {
            customerService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (AssociationExistsException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(path = "balance/{id}")
    public ResponseEntity<CustomerBalanceDto> showCustomerBalance(@PathVariable Long id) {

        Customer customer = customerService.get(id);

        if (customer == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(customerToCustomerBalanceDto.convert(customer), HttpStatus.OK);
    }

    @PutMapping(path = "/{cid}/deposit")
    public ResponseEntity<CustomerDto> deposit(@PathVariable Long cid, @Valid @RequestBody CustomerBalanceDto customerBalanceDto, BindingResult bindingResult) {

        try {

            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            customerService.deposit(cid, Double.parseDouble(customerBalanceDto.getBalance()));

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(path = "/{cid}/withdraw")
    public ResponseEntity<CustomerDto> withdraw(@PathVariable Long cid, @Valid @RequestBody CustomerBalanceDto customerBalanceDto, BindingResult bindingResult) {

        try {

            if (bindingResult.hasErrors()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            customerService.withdraw(cid, Double.parseDouble(customerBalanceDto.getBalance()));

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        } catch (NotEnoughBalanceException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
