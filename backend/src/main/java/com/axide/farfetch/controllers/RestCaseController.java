package com.axide.farfetch.controllers;

import com.axide.farfetch.command.CaseDetailsDto;
import com.axide.farfetch.model.CaseID;
import com.axide.farfetch.services.CaseIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/case")
public class RestCaseController {

    private CaseIdService caseIdService;

    @Autowired
    public void setCaseIdService(CaseIdService caseIdService) {
        this.caseIdService = caseIdService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<CaseDetailsDto> getCaseDetails(@PathVariable Long id) {

        CaseID caseID = caseIdService.getCase(id);


        if (caseID == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        CaseDetailsDto caseDetails = caseIdService.getCaseDetails(id);

        return new ResponseEntity<>(caseDetails, HttpStatus.OK);
    }

}
