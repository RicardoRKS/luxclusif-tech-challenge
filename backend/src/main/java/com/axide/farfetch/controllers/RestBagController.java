package com.axide.farfetch.controllers;


import com.axide.farfetch.command.BagDto;
import com.axide.farfetch.converters.BagDtoToBag;
import com.axide.farfetch.converters.BagToBagDto;
import com.axide.farfetch.converters.ImageHandler;
import com.axide.farfetch.exceptions.BagNotFoundException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.exporter.images.DriveExporter;
import com.axide.farfetch.exporter.spreadsheet.SheetExporter;
import com.axide.farfetch.model.CaseID;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.services.BagService;
import com.axide.farfetch.services.CaseIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/bag")
public class RestBagController {

    private BagService bagService;
    private CaseIdService caseIdService;
    private BagDtoToBag bagDtoToBag;
    private BagToBagDto bagToBagDto;
    private SheetExporter sheetExporter;
    private ImageHandler imageHandler;
    private DriveExporter driveExporter;


    @Autowired
    public void setBagService(BagService bagService) {
        this.bagService = bagService;
    }

    @Autowired
    public void setBagDtoToBag(BagDtoToBag bagDtoToBag) {
        this.bagDtoToBag = bagDtoToBag;
    }

    @Autowired
    public void setBagToBagDto(BagToBagDto bagToBagDto) {
        this.bagToBagDto = bagToBagDto;
    }

    @Autowired
    public void setCaseIdService(CaseIdService caseIdService) {
        this.caseIdService = caseIdService;
    }

    @Autowired
    public void setSheetExporter(SheetExporter sheetExporter) {
        this.sheetExporter = sheetExporter;
    }

    @Autowired
    public void setImageHandler(ImageHandler imageHandler) {
        this.imageHandler = imageHandler;
    }

    @Autowired
    public void setDriveExporter(DriveExporter driveExporter) {
        this.driveExporter = driveExporter;
    }


    @GetMapping(path = "/{id}")
    public ResponseEntity<BagDto> getBag(@PathVariable Long id) {

        Bag bag = bagService.getBag(id);

        if (bag == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(bagToBagDto.convert(bag), HttpStatus.OK);
    }


    @PostMapping(path = {"/", ""})
    public ResponseEntity<CaseID> addBag(@Valid @RequestBody BagDto bagDto, BindingResult bindingResult) {

        CaseID caseID = null;

        if (bindingResult.hasErrors() || bagDto.getBagId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {

            Bag savedBag = bagService.save(bagDto.getCustomerId(), bagDtoToBag.convert(bagDto));
            System.out.println("Bag id: " + savedBag.getId());
            System.out.println("Customer id: " + savedBag.getCustomer().getId());
            caseID = caseIdService.createCase(savedBag);

            System.out.println(caseID.getId());
            System.out.println(caseID.getBagId());

            sheetExporter.export("bag", 0L);
            sheetExporter.export("extras", 0L);
            sheetExporter.export("caseid", 0L);

        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            System.out.println("Export failed");
        }
        return new ResponseEntity<>(caseID, HttpStatus.CREATED);
    }

    @PostMapping(path = {"/image/{caseId}"}, consumes = { "multipart/form-data" })
    public ResponseEntity<?> saveImages(@PathVariable Long caseId, @RequestBody MultipartFile[] file) {
        File exportFile;

        if ((exportFile = imageHandler.imageSaver(file, caseId)) == null) {
            System.err.println("Bad request");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        driveExporter.export(exportFile, caseId);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping(path = "{cid}/{bid}")
    public ResponseEntity<Bag> deleteBag(@PathVariable Long cid, @PathVariable Long bid) {

        try {
            bagService.delete(cid, bid);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        } catch (BagNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
