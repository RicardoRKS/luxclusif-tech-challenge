package com.axide.farfetch.command;

public class CaseIdDto {

    private Long caseId;

    private Long bagId;

    private Long customerId;

    private Long extrasId;

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getBagId() {
        return bagId;
    }

    public void setBagId(Long bagId) {
        this.bagId = bagId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getExtrasId() {
        return extrasId;
    }

    public void setExtrasId(Long extrasId) {
        this.extrasId = extrasId;
    }

    @Override
    public String toString() {
        return "CaseIdDto{" +
                "caseId=" + caseId +
                ", bagId=" + bagId +
                ", customerId=" + customerId +
                ", extrasId=" + extrasId +
                '}';
    }
}
