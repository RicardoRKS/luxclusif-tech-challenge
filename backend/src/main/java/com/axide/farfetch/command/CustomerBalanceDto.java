package com.axide.farfetch.command;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CustomerBalanceDto {


    public static final String moneyRegex = "^\\$?0*[0-9]\\d*(\\.\\d{0,2})?|\\d*(\\.0[1-9])|\\d*(\\.[1-9]\\d?)?$?";

    private Long id;

    @Pattern(regexp = moneyRegex, message = "Amount is not valid")
    @NotNull(message = "Initial amount is mandatory")
    @NotBlank(message = "Initial amount is mandatory")
    private String balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

}
