package com.axide.farfetch.command;

public class CaseDetailsDto {

    private String firstName;
    private String lastName;
    private String email;
    private String brand;
    private String condition;
    private String size;
    private int box;
    private int authenticityCard;
    private int shoulderStrap;
    private int dustbag;
    private int pouch;
    private int padlockAndKey;
    private int bagcharm;
    private int nameTag;
    private int mirror;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getBox() {
        return box;
    }

    public void setBox(int box) {
        this.box = box;
    }

    public int getAuthenticityCard() {
        return authenticityCard;
    }

    public void setAuthenticityCard(int authenticityCard) {
        this.authenticityCard = authenticityCard;
    }

    public int getShoulderStrap() {
        return shoulderStrap;
    }

    public void setShoulderStrap(int shoulderStrap) {
        this.shoulderStrap = shoulderStrap;
    }

    public int getDustbag() {
        return dustbag;
    }

    public void setDustbag(int dustbag) {
        this.dustbag = dustbag;
    }

    public int getPouch() {
        return pouch;
    }

    public void setPouch(int pouch) {
        this.pouch = pouch;
    }

    public int getPadlockAndKey() {
        return padlockAndKey;
    }

    public void setPadlockAndKey(int padlockAndKey) {
        this.padlockAndKey = padlockAndKey;
    }

    public int getBagcharm() {
        return bagcharm;
    }

    public void setBagcharm(int bagcharm) {
        this.bagcharm = bagcharm;
    }

    public int getNameTag() {
        return nameTag;
    }

    public void setNameTag(int nameTag) {
        this.nameTag = nameTag;
    }

    public int getMirror() {
        return mirror;
    }

    public void setMirror(int mirror) {
        this.mirror = mirror;
    }

    @Override
    public String toString() {
        return "CaseDetailsDto{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", brand='" + brand + '\'' +
                ", condition='" + condition + '\'' +
                ", size='" + size + '\'' +
                ", box=" + box +
                ", authenticityCard=" + authenticityCard +
                ", shoulderStrap=" + shoulderStrap +
                ", dustbag=" + dustbag +
                ", pouch=" + pouch +
                ", padlockAndKey=" + padlockAndKey +
                ", bagcharm=" + bagcharm +
                ", nameTag=" + nameTag +
                ", mirror=" + mirror +
                '}';
    }
}
