package com.axide.farfetch.converters;

import com.axide.farfetch.command.BagDto;
import com.axide.farfetch.model.bags.Bag;
import org.springframework.stereotype.Component;

@Component
public class BagToBagDto extends AbstractConverter<Bag, BagDto> {

    @Override
    public BagDto convert(Bag bag) {

        BagDto bagDto = new BagDto();
        bagDto.setBagId(bag.getId());
        bagDto.setBrand(bag.getBrand());
        bagDto.setCondition(bag.getBag_condition());
        bagDto.setSize(bag.getBag_size());
        bagDto.setCustomerId(bag.getCustomer().getId());
        //bagDto.setExtras(bag.getExtrasList().get(0));

        return bagDto;
    }
}
