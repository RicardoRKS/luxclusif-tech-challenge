package com.axide.farfetch.converters;

import com.axide.farfetch.command.CustomerBalanceDto;
import com.axide.farfetch.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerBalanceDto extends AbstractConverter<Customer, CustomerBalanceDto> {


    @Override
    public CustomerBalanceDto convert(Customer customer) {
        CustomerBalanceDto customerBalanceDto = new CustomerBalanceDto();
        customerBalanceDto.setId(customer.getId());
        customerBalanceDto.setBalance(customer.getBalance().toString());

        return customerBalanceDto;
    }
}
