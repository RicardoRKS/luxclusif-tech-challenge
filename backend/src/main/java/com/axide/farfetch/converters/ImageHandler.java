package com.axide.farfetch.converters;


import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class ImageHandler {

    public File imageSaver(MultipartFile[] files, Long caseId) {
        File newFile = null;

        for (MultipartFile file : files) {

            String IMAGE_BASE_PATH = "E:\\Repos\\FarfetchTouchpoint\\backend\\src\\main\\resources\\bag_images\\";
            File imageFolder = new File(IMAGE_BASE_PATH + caseId);

            if (imageFolder.exists() || imageFolder.mkdirs()) {
                newFile = new File(imageFolder.getAbsolutePath() + "\\" + file.getOriginalFilename());
                try {
                    file.transferTo(newFile);
                } catch (IOException e) {
                    return null;
                }
            }
        }
        return newFile;
    }
}
