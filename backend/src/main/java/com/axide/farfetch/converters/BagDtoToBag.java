package com.axide.farfetch.converters;

import com.axide.farfetch.command.BagDto;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.services.BagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class BagDtoToBag implements Converter<BagDto, Bag> {


    private BagService bagService;


    @Autowired
    public void setBagService(BagService bagService) {
        this.bagService = bagService;
    }

    @Override
    public Bag convert(BagDto bagDto) {
        Bag bag = (bagDto.getBagId() != null ? bagService.getBag(bagDto.getBagId()) : new Bag());

        bag.setBrand(bagDto.getBrand());
        bag.setBag_size(bagDto.getSize());
        bag.setBag_condition(bagDto.getCondition());
        bag.setExtrasList(bagDto.getExtras());

        return bag;
    }
}
