package com.axide.farfetch.converters;

import com.axide.farfetch.command.CustomerBalanceDto;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerBalanceDtoToCustomer implements Converter<CustomerBalanceDto, Customer> {

    private CustomerService customerService;

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public Customer convert(CustomerBalanceDto customerBalanceDto) {

        Customer customer = (customerBalanceDto.getId() != null ? customerService.get(customerBalanceDto.getId()) : new Customer());
        customer.setBalance(Double.parseDouble(customerBalanceDto.getBalance()));

        return customer;
    }
}
