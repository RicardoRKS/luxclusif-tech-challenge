package com.axide.farfetch.services;

import com.axide.farfetch.command.CaseDetailsDto;
import com.axide.farfetch.model.CaseID;
import com.axide.farfetch.model.bags.Bag;

public interface CaseIdService {

    CaseID getCase(Long id);

    CaseID createCase(Bag savedBag);

    CaseDetailsDto getCaseDetails(Long id);
}
