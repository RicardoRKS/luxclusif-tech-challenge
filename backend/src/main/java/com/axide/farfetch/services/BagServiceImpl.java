package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.BagNotFoundException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.model.bags.Extras;
import com.axide.farfetch.repositories.BagDao;
import com.axide.farfetch.repositories.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.net.URL;
import java.util.Optional;

@Service
public class BagServiceImpl implements BagService {

    private BagDao bagDao;
    private CustomerDao customerDao;

    @Autowired
    public void setBagDao(BagDao bagDao) {
        this.bagDao = bagDao;
    }

    @Autowired
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }


    @Override
    public void addExtras(Long id, Extras extras) {
        bagDao.findById(id).setExtrasList(extras);
    }

    @Override
    public Bag getBag(Long bid) {
        return bagDao.findById(bid);
    }

    @Override
    @Transactional
    public Bag save(Long cid, Bag bag) throws CustomerNotFoundException {
        Customer customer = Optional.ofNullable(customerDao.findById(cid))
                .orElseThrow(CustomerNotFoundException::new);

        if (!customer.getBagsList().contains(bag)) {
            customer.addToBagsList(bag);
            customerDao.save(customer);
        }
        return customer.getBagsList().get(customer.getBagsList().size() - 1);
    }

    @Override
    @Transactional
    public void delete(Long cid, Long bid) throws CustomerNotFoundException, BagNotFoundException {
        Customer customer = Optional.ofNullable(customerDao.findById(cid))
                .orElseThrow(CustomerNotFoundException::new);

        Bag bag = Optional.ofNullable(bagDao.findById(bid))
                .orElseThrow(BagNotFoundException::new);

        if (!bag.getCustomer().getId().equals(cid)) {
            throw new BagNotFoundException();
        }

        customer.removeBag(bag);
        customerDao.save(customer);
    }
}
