package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.AssociationExistsException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.exceptions.NotEnoughBalanceException;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.repositories.CustomerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {


    private CustomerDao customerDao;


    @Autowired
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    public Customer get(Long id) {
        return customerDao.findById(id);
    }

    @Override
    public Customer getByEmail(String email) {
        return customerDao.findByEmail(email);
    }

    @Override
    public List<Bag> getCustomerBags(Long id) {
        return customerDao.findById(id).getBagsList();
    }

    @Override
    public List<Customer> list() {
        return customerDao.findAll();
    }

    @Override
    public Double getCredit(Long cid) {
        return customerDao.findById(cid).getBalance();
    }

    @Override
    @Transactional
    public void deposit(Long id, Double balance) throws CustomerNotFoundException {
        Customer customer = Optional.ofNullable(customerDao.findById(id))
                .orElseThrow(CustomerNotFoundException::new);

        customer.addCredit(balance);

        customerDao.save(customer);
    }


    @Override
    public void withdraw(Long id, Double balance) throws CustomerNotFoundException, NotEnoughBalanceException {
        Customer customer = Optional.ofNullable(customerDao.findById(id))
                .orElseThrow(CustomerNotFoundException::new);

        if (!customer.removeCredit(balance)) {
            throw new NotEnoughBalanceException();
        }

        customerDao.save(customer);

    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        return customerDao.save(customer);
    }

    @Override
    @Transactional
    public void delete(Long id) throws CustomerNotFoundException, AssociationExistsException {
        Customer customer = Optional.ofNullable(customerDao.findById(id))
                .orElseThrow(CustomerNotFoundException::new);

        if (!customer.getBagsList().isEmpty()) {
            throw new AssociationExistsException();
        }

        customerDao.delete(id);
    }


}
