package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.BagNotFoundException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.model.bags.Extras;

import java.net.URL;

public interface BagService {

    void addExtras(Long bid, Extras extras);

    Bag getBag(Long bid);

    Bag save(Long cid, Bag bag) throws CustomerNotFoundException;

    void delete(Long cid, Long bid) throws CustomerNotFoundException, BagNotFoundException;

}
