package com.axide.farfetch.services;

import com.axide.farfetch.exceptions.AssociationExistsException;
import com.axide.farfetch.exceptions.CustomerNotFoundException;
import com.axide.farfetch.exceptions.NotEnoughBalanceException;
import com.axide.farfetch.model.Customer;
import com.axide.farfetch.model.bags.Bag;

import java.util.List;

public interface CustomerService {

    Customer get(Long id);

    Customer getByEmail(String email);

    List<Bag> getCustomerBags(Long id);

    List<Customer> list();

    Double getCredit(Long id);

    void deposit(Long id, Double balance) throws CustomerNotFoundException;

    void withdraw(Long id, Double balance) throws CustomerNotFoundException, NotEnoughBalanceException;

    Customer save(Customer customer);

    void delete(Long bid) throws CustomerNotFoundException, AssociationExistsException;
}
