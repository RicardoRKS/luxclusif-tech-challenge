package com.axide.farfetch.services;

import com.axide.farfetch.command.CaseDetailsDto;
import com.axide.farfetch.model.CaseID;
import com.axide.farfetch.model.bags.Bag;
import com.axide.farfetch.repositories.CaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CaseIdImpl implements CaseIdService {

    private CaseDao caseDao;

    @Autowired
    public void setCaseDao(CaseDao caseDao) {
        this.caseDao = caseDao;
    }

    @Override
    public CaseID getCase(Long id) {
        return caseDao.findById(id);
    }

    @Override
    @Transactional
    public CaseID createCase(Bag savedBag) {

        CaseID caseID = new CaseID();

        caseID.setBagId(savedBag.getId());
        caseID.setCustomerId(savedBag.getCustomer().getId());
        caseID.setExtrasId(savedBag.getExtrasList().get(0).getId());
        caseID = caseDao.save(caseID);

        return caseID;
    }

    @Override
    public CaseDetailsDto getCaseDetails(Long id) {
        return caseDao.getCaseDetails(id);
    }

}
